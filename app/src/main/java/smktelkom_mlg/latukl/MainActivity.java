package smktelkom_mlg.latukl;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import smktelkom_mlg.latukl.koneksi.VolleySingleton;
import smktelkom_mlg.latukl.model.BarangModel;
import smktelkom_mlg.latukl.model.UserModel;
import smktelkom_mlg.latukl.session.SessionPreferences;

public class MainActivity extends AppCompatActivity implements Spinner.OnItemSelectedListener {
    public static final String TAG_CATEGORY = "Category";
    private static final String JSON_ARRAY = "result";
    private final String urlKategori = " http://192.168.0.109/rest_ukl/index.php/kategori";
    private final String urlJenis = " http://192.168.0.109/rest_ukl/index.php/jenis";
    private final String urlBarang = " http://192.168.0.109/rest_ukl/index.php/barang";
    private TextView nama;
    private TextView txtID, txtAset, txtSpec, txtTgl, txtRuang, txtCat, txtJenis, txtHarga, txtInv, txtSatuan;
    private EditText etAset, etSpec, etTgl, etRuang, etHarga, etSatuan;
    private TableLayout tabLayout;
    private Button logout, insert;
    private Spinner spKategori, spJenis;
    private ArrayList<String> kategori = new ArrayList<>();
    private ArrayList<String> elektronik = new ArrayList<>();
    private ArrayList<String> nonelektronik = new ArrayList<>();
    private Calendar myCalendar;
    private String idjenis;
    private DatePickerDialog.OnDateSetListener date;
    private JSONArray result;
    private UserModel user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (!SessionPreferences.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }
        nama = (TextView) findViewById(R.id.nama);
        spKategori = (Spinner) findViewById(R.id.spinnerKat);
        spJenis = (Spinner) findViewById(R.id.spinnerJenis);
        etAset = (EditText) findViewById(R.id.etAset);
        etSpec = (EditText) findViewById(R.id.etSpec);
        etTgl = (EditText) findViewById(R.id.etTgl);
        etRuang = (EditText) findViewById(R.id.etRuang);
        etHarga = (EditText) findViewById(R.id.etHarga);
        etSatuan = (EditText) findViewById(R.id.etSatuan);
        tabLayout = (TableLayout) findViewById(R.id.tableLayout);
        spKategori.setOnItemSelectedListener(this);
        user = SessionPreferences.getInstance(this).getUser();
        nama.setText(user.getNama());
        logout = (Button) findViewById(R.id.logout);
        insert = (Button) findViewById(R.id.insert);
        myCalendar = Calendar.getInstance();
        date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        etTgl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(MainActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                InserData();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                SessionPreferences.getInstance(getApplicationContext()).logout();
            }
        });
        LoadKategori();
        LoadInventaris();
        LoadJenis();
    }


    private void updateLabel() {
        String myFormat = "yyyy-mm-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        etTgl.setText(sdf.format(myCalendar.getTime()));
    }


    private void LoadInventaris() {
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlBarang, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);

                        BarangModel barangModel = new BarangModel(
                                obj.getInt("no_aset"),
                                obj.getInt("id_jenis"),
                                obj.getInt("hrg_satuan"),
                                obj.getString("no_inventaris"),
                                obj.getString("nama_aset"),
                                obj.getString("spesifikasi"),
                                obj.getString("nama_ruangan"),
                                obj.getString("tgl_pengadaan"),
                                obj.getString("id_kategori"),
                                obj.getString("satuan")
                        );
                        Log.d("ID Inventaris", String.valueOf(barangModel.getId()));
                        View tableRow = LayoutInflater.from(MainActivity.this).inflate(R.layout.table_item, null, false);
                        txtID = (TextView) tableRow.findViewById(R.id.txtID);
                        txtInv = (TextView) tableRow.findViewById(R.id.txtInv);
                        txtAset = (TextView) tableRow.findViewById(R.id.txtAset);
                        txtSpec = (TextView) tableRow.findViewById(R.id.txtSpec);
                        txtRuang = (TextView) tableRow.findViewById(R.id.txtRuang);
                        txtTgl = (TextView) tableRow.findViewById(R.id.txtTgl);
                        txtCat = (TextView) tableRow.findViewById(R.id.txtCategory);
                        txtJenis = (TextView) tableRow.findViewById(R.id.txtJenis);
                        txtHarga = (TextView) tableRow.findViewById(R.id.txtHarga);
                        txtSatuan = (TextView) tableRow.findViewById(R.id.txtSatuan);

                        String ID = String.valueOf(barangModel.getId());
                        String Harga = String.valueOf(barangModel.getHarga());
                        String Jenis = String.valueOf(barangModel.getJenis());

                        txtID.setText(ID);
                        txtInv.setText(barangModel.getInvent());
                        txtAset.setText(barangModel.getAset());
                        txtSpec.setText(barangModel.getSpec());
                        txtTgl.setText(barangModel.getTgl());
                        txtRuang.setText(barangModel.getRuang());
                        txtCat.setText(barangModel.getCategory());
                        txtJenis.setText(Jenis);
                        txtHarga.setText(Harga);
                        txtSatuan.setText(barangModel.getSatuan());

                        tabLayout.addView(tableRow);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public void LoadKategori() {
        //Creating a string request
        StringRequest stringRequest = new StringRequest(urlKategori,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        JSONArray j = null;
                        try {
                            j = new JSONArray(response);
                            result = j;
                            getKategori(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void LoadJenis() {
        elektronik.clear();
        nonelektronik.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.GET, urlJenis, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray j = new JSONArray(response);
                    for (int i = 0; i < j.length(); i++) {
                        JSONObject obj = j.getJSONObject(i);
                        if (obj.getString("IDCategory").equals("A")) {
                            elektronik.add(obj.getString("Type"));
                            idjenis = obj.getString("IDType");
                        } else if (obj.getString("IDCategory").equals("B")) {
                            nonelektronik.add(obj.getString("Type"));
                            idjenis = obj.getString("IDType");
                        }
                    }
                    Log.d("Elektronik", elektronik.toString());
                    Log.d("NonElektronik", nonelektronik.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void getKategori(JSONArray result) {
        for (int i = 0; i < result.length(); i++) {
            try {
                JSONObject json = result.getJSONObject(i);

                kategori.add(json.getString(TAG_CATEGORY));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Setting adapter to show the items in the spinner
        spKategori.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, kategori));
    }

    private void InserData() {
        String branch = user.getBranch();
        String kategori = spKategori.getSelectedItem().toString();
        String id = null;
        if (kategori.equals("elektronik")) {
            id = "A";
        } else if (kategori.equals("non elektronik")) {
            id = "B";
        }
        String jenis = spJenis.getSelectedItem().toString();
        String tgl = etTgl.getText().toString().trim();
        String date[] = tgl.split("-");
        String noInv = branch + date[0] + date[1] + id + idjenis + "/" + "rid";
        etAset.setText(noInv);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selectedItem = parent.getItemAtPosition(position).toString();
        switch (selectedItem) {
            case "elektronik":
                setAdapter(0);
                break;
            case "non elektronik":
                setAdapter(1);
                break;
        }

    }

    private void setAdapter(int i) {
        switch (i) {
            case 0:
                spJenis.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, elektronik));
                break;
            case 1:
                spJenis.setAdapter(new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_dropdown_item, nonelektronik));
                break;
        }

    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }


}
