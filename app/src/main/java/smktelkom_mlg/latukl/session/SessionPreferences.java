package smktelkom_mlg.latukl.session;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import smktelkom_mlg.latukl.LoginActivity;
import smktelkom_mlg.latukl.model.UserModel;

/**
 * Created by MirzaUY on 5/5/2018.
 */

public class SessionPreferences {
    private static final String SHARED_PREFERENCES_NAME = "sessionsharedpref";
    private static final String KEY_ID = "keyid";
    private static final String KEY_NAMA = "keynama";
    private static final String KEY_USERNAME = "keyusername";
    private static final String KEY_PASSWORD = "keypass";
    private static final String KEY_BRANCH = "keybranch";
    private static SessionPreferences sessionPreferences = null;
    private static Context context;

    private SessionPreferences(Context ctx) {
        context = ctx;
    }

    public static synchronized SessionPreferences getInstance(Context ctx) {
        if (sessionPreferences == null) {
            sessionPreferences = new SessionPreferences(ctx);
        }
        return sessionPreferences;
    }

    public void userLogin(UserModel user) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_ID, user.getId());
        editor.putString(KEY_NAMA, user.getNama());
        editor.putString(KEY_USERNAME, user.getUsername());
        editor.putString(KEY_PASSWORD, user.getPassword());
        editor.putString(KEY_BRANCH, user.getBranch());
        editor.apply();
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return sharedPreferences.getString(KEY_USERNAME, null) != null;
    }

    public UserModel getUser() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        return new UserModel(
                sharedPreferences.getInt(KEY_ID, -1),
                sharedPreferences.getString(KEY_NAMA, null),
                sharedPreferences.getString(KEY_USERNAME, null),
                sharedPreferences.getString(KEY_PASSWORD, null),
                sharedPreferences.getString(KEY_BRANCH, null)
        );
    }

    public void logout() {
        SharedPreferences sharedPreferences = context.getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        context.startActivity(new Intent(context, LoginActivity.class));

    }


}
