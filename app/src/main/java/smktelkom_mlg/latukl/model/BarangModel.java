package smktelkom_mlg.latukl.model;

/**
 * Created by MirzaUY on 5/5/2018.
 */

public class BarangModel {
    private int id, jenis, harga;
    private String invent;
    private String aset;
    private String spec;
    private String ruang;
    private String tgl;
    private String category;
    private String satuan;

    public BarangModel(int id, int jenis, int harga, String invent, String aset, String spec, String ruang, String tgl, String category, String satuan) {
        this.id = id;
        this.jenis = jenis;
        this.harga = harga;
        this.invent = invent;
        this.aset = aset;
        this.spec = spec;
        this.ruang = ruang;
        this.tgl = tgl;
        this.category = category;
        this.satuan = satuan;
    }

    public int getId() {
        return id;
    }

    public int getJenis() {
        return jenis;
    }

    public int getHarga() {
        return harga;
    }

    public String getInvent() {
        return invent;
    }

    public String getAset() {
        return aset;
    }

    public String getSpec() {
        return spec;
    }

    public String getRuang() {
        return ruang;
    }

    public String getTgl() {
        return tgl;
    }

    public String getCategory() {
        return category;
    }

    public String getSatuan() {
        return satuan;
    }


}
