package smktelkom_mlg.latukl.model;

/**
 * Created by MirzaUY on 5/5/2018.
 */

public class UserModel {
    private int id;
    private String nama;
    private String username;
    private String password;
    private String branch;


    public UserModel(int id, String nama, String username, String password, String branch) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.branch = branch;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getBranch() {
        return branch;
    }


}
