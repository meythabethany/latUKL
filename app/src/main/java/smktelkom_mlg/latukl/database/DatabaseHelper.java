package smktelkom_mlg.latukl.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by MirzaUY on 5/6/2018.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Inventaris.db";
    private static final String TABLE_NAME = "barang";
    private static final String COLUMN_NO_ASET = "no_aset";
    private static final String COLUMN_NO_INVENTARIS = "no_inventaris";
    private static final String COLUMN_NAMA_ASET = "nama_aset";
    private static final String COLUMN_SPESIFIKASI = "spesifikasi";
    private static final String COLUMN_TGL = "tgl_pengadaan";
    private static final String COLUMN_RUANG = "nama_ruangan";
    private static final String COLUMN_ID_KATEGORI = "id_kategori";
    private static final String COLUMN_ID_JENIS = "id_jenis";
    private static final String COLUMN_HARGA = "hrg_satuan";
    private static final String COLUMN_SATUAN = "satuan";
    private static final String CREATE_TABLE = "CREATE TABLE barang(no_aset integer primary key not null auto_increment,no_inventaris varchar(100), " +
            "spesifikasi text, tgl_pengadaan date, nama_ruangan varchar(200), id_kategori varchar;";
    SQLiteDatabase db;


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
        this.db = db;
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query = "DROP TABLE IF EXISTS" + TABLE_NAME;
        db.execSQL(query);
        this.onCreate(db);
    }
}
