package smktelkom_mlg.latukl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import smktelkom_mlg.latukl.koneksi.VolleySingleton;
import smktelkom_mlg.latukl.model.UserModel;
import smktelkom_mlg.latukl.session.SessionPreferences;

public class LoginActivity extends AppCompatActivity {
    private final String url = " http://192.168.0.109/rest_ukl/index.php/users";
    private EditText etUser, etPass;
    private Button btLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        etUser = (EditText) findViewById(R.id.etUsername);
        etPass = (EditText) findViewById(R.id.etPass);
        btLogin = (Button) findViewById(R.id.btLogin);
        if (SessionPreferences.getInstance(this).isLoggedIn()) {
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginUser();
            }
        });

    }

    private void LoginUser() {
        final String username = etUser.getText().toString();
        final String password = etPass.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        String user = obj.getString("UserName");
                        String pass = obj.getString("Password");
                        Log.d("data", "onResponse: " + user + pass);
                        if (username.equals(user) && password.equals(pass)) {
                            UserModel userModel = new UserModel(
                                    obj.getInt("IDUser"),
                                    obj.getString("Name"),
                                    obj.getString("UserName"),
                                    obj.getString("Password"),
                                    obj.getString("Branch")
                            );
                            SessionPreferences.getInstance(getApplicationContext()).userLogin(userModel);
                            startActivity(new Intent(getApplicationContext(), MainActivity.class));
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}
